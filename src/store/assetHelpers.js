import Vue from 'vue'

export const makeDeleteChildFromParentMutation = ({parent, child}) =>
  (state, {childId, parentId}) => {
    const resource = state.items[parentId]
    if (resource[child]) {
      Vue.delete(resource[child], childId)
    }
  }

export const makeAppendChildToParentMutation = ({parent, child}) =>
  (state, {childId, parentId}) => {
    const resource = state.items[parentId]
    if (!resource[child]) {
      Vue.set(resource, child, {})
    }
    Vue.set(resource[child], childId, childId)
  }
