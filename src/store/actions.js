import firebase from 'firebase'

export default {
  fetchItem ({state, commit}, {id, emoji, resource}) {
    console.log('🔥‍', emoji, id)
    return new Promise((resolve, reject) => {
      firebase.database().ref(resource).child(id).once('value', snapshot => {
        // console.log('snapshot‍', {resource, id: snapshot.key, item: snapshot.val()})
        commit('setItem', {resource, id: snapshot.key, item: snapshot.val()})
        resolve(state[resource].items[id])
      })
    })
  },

  fetchItems ({dispatch}, {ids, resource, emoji}) {
    // eslint-disable-next-line prefer-promise-reject-errors
    if (!ids) return Promise.reject('ids not found')
    ids = Array.isArray(ids) ? ids : Object.keys(ids)
    return Promise.all(ids.map(id => dispatch('fetchItem', {id, resource, emoji})))
  }
}
