import Vue from 'vue'
import firebase from 'firebase'

export default {
  namespaced: true,

  state: {
    items: {}
  },

  actions: {
    createPost ({commit, state, rootState}, post) {
      const postId = firebase.database().ref('posts').push().key
      post.userId = rootState.auth.authId
      post.publishedAt = Math.floor(Date.now() / 1000)

      const updates = {}
      updates[`posts/${postId}`] = post
      updates[`threads/${post.threadId}/posts/${postId}`] = postId
      updates[`threads/${post.threadId}/contributors/${post.userId}`] = post.userId
      updates[`users/${post.userId}/posts/${postId}`] = postId
      firebase.database().ref().update(updates)
        .then(() => {
          commit('setItem', {resource: 'posts', item: post, id: postId}, {root: true})
          commit('threads/appendPostToThread', {parentId: post.threadId, childId: postId}, {root: true})
          commit('threads/appendContributorToThread', {parentId: post.threadId, childId: post.userId}, {root: true})
          commit('users/appendPostToUser', {parentId: post.userId, childId: postId}, {root: true})
          return Promise.resolve(state.items[postId])
        })
    },

    deletePost ({state, commit, rootState}, {post}) {
      return new Promise((resolve, reject) => {
        firebase.database().ref(`posts/${post['.key']}`).remove()
          .then(() => firebase.database().ref(`users/${post.userId}/posts/${post['.key']}`).remove())
          .then(() => firebase.database().ref(`threads/${post.threadId}/posts/${post['.key']}`).remove())
          .then(() => {
            commit('deleteItem', {id: post['.key'], resource: 'posts'}, {root: true})
            commit('threads/deletePostFromThread', {parentId: post.threadId, childId: post['.key']}, {root: true})
            commit('users/deletePostFromUser', {parentId: post.userId, childId: post['.key']}, {root: true})
            const threadPosts = Object.keys(rootState.threads.items[post.threadId].posts)
            const userPosts = Object.keys(rootState.users.items[post.userId].posts)
            const hasPost = threadPosts.some(item => userPosts.includes(item))
            if (!hasPost) {
              commit('threads/deleteContributorFromThread', {parentId: post.threadId, childId: post.userId}, {root: true})
            }
            resolve('Вы успешно удалили пост!')
          })
      })
    },

    updatePost ({state, commit, rootState}, {id, text}) {
      return new Promise((resolve, reject) => {
        const post = state.items[id]
        const edited = {
          at: Math.floor(Date.now() / 1000),
          by: rootState.auth.authId
        }

        const updates = {text, edited}
        firebase.database().ref('posts').child(id).update(updates)
          .then(() => {
            commit('setPost', {postId: id, post: {...post, text, edited}})
            resolve(post)
          })
      })
    },

    fetchPost: ({dispatch}, {id}) => dispatch('fetchItem', {resource: 'posts', id, emoji: '💬'}, {root: true}),
    fetchPosts: ({dispatch}, {ids}) => dispatch('fetchItems', {resource: 'posts', ids, emoji: '💬'}, {root: true})
  },

  mutations: {
    setPost (state, {post, postId}) {
      Vue.set(state.items, postId, post)
    }
  }
}
