// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import firebase from 'firebase'
import App from './App'
import router from './router'
import store from '@/store'
import AppDate from '@/components/AppDate'

Vue.component('AppDate', AppDate)

Vue.config.productionTip = false

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: 'AIzaSyCB_folUYWp3F1kFwwapCnyPwH7s2Ee48o',
  authDomain: 'forum-6a749.firebaseapp.com',
  databaseURL: 'https://forum-6a749.firebaseio.com',
  projectId: 'forum-6a749',
  storageBucket: 'forum-6a749.appspot.com',
  messagingSenderId: '988140958157',
  appId: '1:988140958157:web:eaaf0a133279903d661484'
}
// Initialize Firebase
firebase.initializeApp(firebaseConfig)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
